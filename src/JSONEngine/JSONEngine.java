
package JSONEngine;

import com.google.gson.Gson;

/**
 *
 * @author Ahsan (muhammad.ahsan@gmail.com)
 */
public class JSONEngine {
    public static void main(String[] args) {
        
        /* Similar to object serialization and deserialization. 
         * Conversion from Java object to JSON and vice versa. 
         * Can be used for web services with low latency overhead 
         */
        String jsonized = new Gson().toJson( new Car()); 
        System.out.println(jsonized);
        Car Creceived = new Gson().fromJson("{\"manufacturer\":\"Honda\","
                + "\"color\":\"Black\",\"dimentions\":50}", Car.class);
        System.out.println(Creceived);
    }
}
